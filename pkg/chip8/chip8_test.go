package chip8

import (
	"testing"
)

func TestInitializeSucessful(t *testing.T) {
	cpu, err := initialize()
	if err != nil {
		t.Error(err)
	} else {
		println(cpu.memory[0])
	}
}

func TestLoadRom(t *testing.T) {
	cpu, err := initialize()

	if err != nil {
		t.Error(err)
	}
	err = cpu.loadRom("res/pong.rom")

}

func TestExecuteCycle(t *testing.T) {
	cpu, err := initialize()

	if err != nil {
		t.Error(err)
	}
	err = cpu.loadRom("res/pong.rom")

	for i := 0; i < 50; i++ {
		cpu.emulateCycle()
	}
}
