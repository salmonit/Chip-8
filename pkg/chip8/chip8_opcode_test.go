package chip8

import (
	"math/rand"
	"testing"
	"time"
)

func TestClearDisplay(t *testing.T) {
	chip8, _ := initialize()
	chip8.Vram = generateRandomArray()

	chip8.memory[0x201] = 0xE0

	chip8.emulateCycle()

	for i := 0; i < 64*32; i++ {
		if chip8.Vram[i] != 0 {
			t.Error("Vram not zero")
			break
		}
	}
}

func generateRandomArray() [64 * 32]uint8 {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	var array [64 * 32]uint8

	for i := 0; i < 64*32; i++ {
		array[i] = uint8(r1.Intn(255))
	}

	return array
}

func TestReturnFromSubroutine(t *testing.T) {

	chip8, _ := initialize()
	chip8.stack[1] = 0x400
	chip8.sp++

	chip8.memory[0x201] = 0xEE

	chip8.emulateCycle()

	if chip8.pc != 0x402 {
		t.Error("Returned to wrong address")
	}
}

func TestJumpToLocation(t *testing.T) {
	chip8, _ := initialize()

	chip8.memory[0x200] = 0x1A
	chip8.memory[0x201] = 0x58

	chip8.emulateCycle()

	if chip8.pc != 0xA58 {
		t.Errorf("Jumped to wrong address: 0x%x\n", chip8.pc)
	}

}

func TestCallSubroutine(t *testing.T) {
	chip8, _ := initialize()

	chip8.memory[0x200] = 0x2A
	chip8.memory[0x201] = 0x58

	chip8.emulateCycle()

	if chip8.pc != 0xA58 {
		t.Errorf("Jumped to wrong address: 0x%x\n", chip8.pc)
	}

	if chip8.sp == 0 {
		t.Error("Stack pointer should not be 0")
	}

	if chip8.stack[chip8.sp] != 0x200 {
		t.Errorf("Wrong return address on stack: 0x%x\n", chip8.stack[chip8.sp])
	}

}

func TestSETrue(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA

	chip8.memory[0x200] = 0x30
	chip8.memory[0x201] = 0xAA

	chip8.emulateCycle()

	if chip8.pc != 0x204 {
		t.Error("Didn't skipped next execution, but should have skipped")
	}
}

func TestSEFalse(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA

	chip8.memory[0x200] = 0x30
	chip8.memory[0x201] = 0xAB

	chip8.emulateCycle()

	if chip8.pc != 0x202 {
		t.Error("Skipped next execution, but shouldn't have skipped")
	}
}

func TestSNETrue(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA

	chip8.memory[0x200] = 0x40
	chip8.memory[0x201] = 0xAB

	chip8.emulateCycle()

	if chip8.pc != 0x204 {
		t.Error("Didn't skipped next execution, but should have skipped")
	}
}

func TestSNEFalse(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA

	chip8.memory[0x200] = 0x40
	chip8.memory[0x201] = 0xAA

	chip8.emulateCycle()

	if chip8.pc != 0x202 {
		t.Error("Skipped next execution, but shouldn't have skipped")
	}
}

func TestSERegTrue(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA
	chip8.register[1] = 0xAA

	chip8.memory[0x200] = 0x50
	chip8.memory[0x201] = 0x10

	chip8.emulateCycle()

	if chip8.pc != 0x204 {
		t.Error("Didn't skipped next execution, but should have skipped")
	}
}

func TestSERegFalse(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA
	chip8.register[1] = 0xAB

	chip8.memory[0x200] = 0x50
	chip8.memory[0x201] = 0x10

	chip8.emulateCycle()

	if chip8.pc != 0x202 {
		t.Error("Skipped next execution, but shouldn't have skipped")
	}
}

func TestSNERegTrue(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA
	chip8.register[1] = 0xAB

	chip8.memory[0x200] = 0x90
	chip8.memory[0x201] = 0x10

	chip8.emulateCycle()

	if chip8.pc != 0x204 {
		t.Error("Didn't skipped next execution, but should have skipped")
	}
}

func TestSNERegFalse(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 0xAA
	chip8.register[1] = 0xAA

	chip8.memory[0x200] = 0x90
	chip8.memory[0x201] = 0x10

	chip8.emulateCycle()

	if chip8.pc != 0x202 {
		t.Error("Skipped next execution, but shouldn't have skipped")
	}
}

func TestLD(t *testing.T) {

	chip8, _ := initialize()

	chip8.memory[0x200] = 0x65
	chip8.memory[0x201] = 0xAB

	chip8.emulateCycle()

	if chip8.register[5] != 0xAB {
		t.Errorf("Loaded wrong value int register")
	}

}

func TestADD(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[5] = 0x1

	chip8.memory[0x200] = 0x75
	chip8.memory[0x201] = 0xAB

	chip8.emulateCycle()

	if chip8.register[5] != 0xAC {
		t.Errorf("Addition have wrong result")
	}
}

func TestLDReg(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 5

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x20

	chip8.emulateCycle()

	if chip8.register[1] != 5 {
		t.Error("Wrong value in register")
	}
}

func TestOrReg(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 5

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x21

	chip8.emulateCycle()

	if chip8.register[1] != 5 {
		t.Error("Wrong value in register")
	}
}

func TestAndReg(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 5

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x22

	chip8.emulateCycle()

	if chip8.register[1] != 0 {
		t.Error("Wrong value in register")
	}
}

func TestXorReg(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 0x5
	chip8.register[1] = 0xA

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x23

	chip8.emulateCycle()

	if chip8.register[1] != 0xF {
		t.Error("Wrong value in register")
	}
}

func TestAddRegNoCarry(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 0x3
	chip8.register[1] = 0xA

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x24

	chip8.emulateCycle()

	if chip8.register[1] != 0xD {
		t.Error("Wrong value in register")
	}
}

func TestAddRegCarry(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 0xFA
	chip8.register[1] = 0xAA

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x24

	chip8.emulateCycle()

	if chip8.register[1] != 0xA4 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 1 {
		t.Error("Carry not set")
	}
}

func TestSubRegNoBorrow(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 0x1
	chip8.register[1] = 0x5

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x25

	chip8.emulateCycle()

	if chip8.register[1] != 0x4 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 1 {
		t.Error("Borrow is set")
	}
}

func TestSubRegBorrow(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[2] = 0x5
	chip8.register[1] = 0x1

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x25

	chip8.emulateCycle()

	if chip8.register[1] != 0xFC {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 0 {
		t.Error("Carry not set")
	}
}

func TestSHRVFSet(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[1] = 0x1

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x26

	chip8.emulateCycle()

	if chip8.register[1] != 0x0 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 1 {
		t.Error("Borrow not set")
	}
}

func TestSHRVFNotSet(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[1] = 0x2

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x26

	chip8.emulateCycle()

	if chip8.register[1] != 0x1 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 0 {
		t.Error("Borrow set")
	}
}

func TestSubnRegNoBorrow(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[1] = 0x2
	chip8.register[2] = 0x3

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x27

	chip8.emulateCycle()

	if chip8.register[1] != 0x1 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 1 {
		t.Error("Borrow not set")
	}
}

func TestSubnRegBorrow(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[1] = 0x3
	chip8.register[2] = 0x2

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x27

	chip8.emulateCycle()

	if chip8.register[1] != 0xFF {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 0 {
		t.Error("Borrow set")
	}
}

func TestSHLVFSet(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[1] = 0x80

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x2E

	chip8.emulateCycle()

	if chip8.register[1] != 0x0 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 1 {
		t.Error("Borrow not set")
	}
}

func TestSHLVFNotSet(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[1] = 0x2

	chip8.memory[0x200] = 0x81
	chip8.memory[0x201] = 0x2E

	chip8.emulateCycle()

	if chip8.register[1] != 0x4 {
		t.Error("Wrong value in register")
	}

	if chip8.register[0xF] != 0 {
		t.Error("Borrow set")
	}
}

func TestSetDelayTimer(t *testing.T) {
	chip8, _ := initialize()

	chip8.register[0] = 0xAB

	chip8.memory[0x200] = 0xF0
	chip8.memory[0x201] = 0x15

	chip8.emulateCycle()

	if chip8.delayTimer != 0xAB {
		t.Error("Delay timer have wrong value")
	}
}

func TestStoreDelayTimer(t *testing.T) {

	chip8, _ := initialize()

	chip8.delayTimer = 0x03

	chip8.memory[0x200] = 0xF0
	chip8.memory[0x201] = 0x07

	chip8.emulateCycle()

	if chip8.register[0] != 0x03 {
		t.Error("Delay timer have wrong value")
	}

}

func TestSKP(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 5
	chip8.Keys[5] = 1

	chip8.memory[0x200] = 0xE0
	chip8.memory[0x201] = 0x9E

	chip8.emulateCycle()

	if chip8.pc != 0x204 {
		t.Error("Skip not executed")
	}
}

func TestSKNP(t *testing.T) {

	chip8, _ := initialize()

	chip8.register[0] = 5

	chip8.memory[0x200] = 0xE0
	chip8.memory[0x201] = 0x1A

	chip8.emulateCycle()

	if chip8.pc != 0x204 {
		t.Error("Skip not executed")
	}
}

/*func TestRandom(t *testing.T) {

	chip8, _ := initialize()

	chip8.memory[0x200] = 0xC0
	chip8.memory[0x201] = 0x07

	chip8.emulateCycle()

	if chip8.register[0] >= 0 {
		t.Error("Random didn't worked as expected")
	}

}*/
