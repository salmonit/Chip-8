package chip8

import (
	"fmt"
	"io/ioutil"
	"math/rand"
)

const SCREEN_WIDTH = 64
const SCREEN_HEIGHT = 32

var Chip8Fontset = [80]byte{
	0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
	0x20, 0x60, 0x20, 0x20, 0x70, // 1
	0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
	0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
	0x90, 0x90, 0xF0, 0x10, 0x10, // 4
	0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
	0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
	0xF0, 0x10, 0x20, 0x40, 0x40, // 7
	0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
	0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
	0xF0, 0x90, 0xF0, 0x90, 0x90, // A
	0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
	0xF0, 0x80, 0x80, 0x80, 0xF0, // C
	0xE0, 0x90, 0x90, 0x90, 0xE0, // D
	0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
	0xF0, 0x80, 0xF0, 0x80, 0x80, // F
}

type Chip8 struct {
	opcode     uint16
	memory     [4096]uint8
	register   [16]uint8
	I          uint16
	pc         uint16
	sp         uint16
	stack      [16]uint16
	Vram       [64 * 32]uint8
	Keys       [16]uint8
	delayTimer uint8
	soundTimer uint8
	DrawFlag   bool
}

func (*Chip8) Initialize() *Chip8 {

	cpu := Chip8{pc: 0x200, opcode: 0, I: 0, sp: 0}

	// Initalize the fontset
	for i := 0; i < len(Chip8Fontset); i++ {
		cpu.memory[i] = Chip8Fontset[i]
	}

	return &cpu
}

func (cpu *Chip8) LoadRom(path string) bool {
	file, err := ioutil.ReadFile(path)

	if err != nil {
		fmt.Println(err)
		return false
	}

	if len(file) > len(cpu.memory) {
		fmt.Println("File is bigger then memory")
		return false
	}

	for i := 0; i < len(file); i++ {
		cpu.memory[i+0x200] = file[i]
	}

	return true
}

func (cpu *Chip8) EmulateCycle() {

	cpu.opcode = uint16(cpu.memory[cpu.pc])<<8 | uint16(cpu.memory[cpu.pc+1])

	switch cpu.opcode & 0xF000 {
	case 0x0000:
		switch cpu.opcode & 0x00FF {
		case 0xE0: // CLS
			for i := 0; i < 64*32; i++ {
				cpu.Vram[i] = 0
			}
			// fmt.Println("Cleared display")
			cpu.pc += 2
			break
		case 0xEE: // CLR
			cpu.pc = cpu.stack[cpu.sp] + 2
			cpu.sp--
			// fmt.Printf("Returned to 0x%x\n", cpu.pc)
			break
		default:
			// fmt.Printf("Unkown opcode: 0x%x\n", cpu.opcode)
		}
	case 0x1000: // JP addr
		var jumpLocation = cpu.opcode & 0x0FFF
		cpu.pc = jumpLocation
		// fmt.Printf("Jumped to location 0x%x\n", jumpLocation)
		break
	case 0x2000: // CALL addr
		var callLocation = cpu.opcode & 0x0FFF
		cpu.sp++
		cpu.stack[cpu.sp] = cpu.pc
		cpu.pc = callLocation
		// fmt.Printf("Called subroutine at 0x%x\n", callLocation)
		break
	case 0x3000: // SE Vx, byte (Skip Equal)
		var x = uint16(cpu.opcode&0x0F00) >> 8
		var kk = uint8(cpu.opcode & 0x00FF)
		if cpu.register[x] == kk {
			cpu.pc += 4
		} else {
			cpu.pc += 2
		}
		break
	case 0x4000: // SNE Vx, byte (Skip Not Equal)
		var x = uint16(cpu.opcode&0x0F00) >> 8
		var kk = uint8(cpu.opcode & 0x00FF)
		if cpu.register[x] != kk {
			cpu.pc += 4
		} else {
			cpu.pc += 2
		}
		break
	case 0x5000: // SE Vx, Vy (Skip Equal)
		var x = uint16(cpu.opcode&0x0F00) >> 8
		var y = uint16(cpu.opcode&0x00F0) >> 4
		if cpu.register[x] == cpu.register[y] {
			cpu.pc += 4
		} else {
			cpu.pc += 2
		}
		break
	case 0x6000: // LD Vx, byte
		var vx = uint16(cpu.opcode&0x0F00) >> 8
		cpu.register[vx] = uint8(cpu.opcode & 0x00FF)
		// fmt.Printf("Loaded %d into Register V%X\n", cpu.register[vx], vx)
		cpu.pc += 2
		break
	case 0x7000: // ADD Vx, byte
		var vx = (cpu.opcode & 0x0F00) >> 8
		var kk = cpu.opcode & 0xFF
		cpu.register[vx] += uint8(kk)
		cpu.pc += 2
		// fmt.Printf("Added %d into register V%x\n", kk, vx)
		break
	case 0x8000:
		switch cpu.opcode & 0x000F {
		case 0x0: // LD Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4
			cpu.register[x] = cpu.register[y]
			// fmt.Printf("Loaded 0x%x from V%d into V%d", cpu.register[y], y, x)
			cpu.pc += 2
			break
		case 0x1: // OR Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4
			cpu.register[x] |= cpu.register[y]
			// fmt.Printf("OR")
			cpu.pc += 2
			break
		case 0x2: // AND Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4
			cpu.register[x] &= cpu.register[y]
			// fmt.Printf("AND")
			cpu.pc += 2
			break
		case 0x3: // XOR Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4
			cpu.register[x] ^= cpu.register[y]
			// fmt.Printf("XOR")
			cpu.pc += 2
			break
		case 0x4: // ADD Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4

			cpu.register[0xF] = 0
			if (uint16(cpu.register[x]) + uint16(cpu.register[y])) > 255 {
				cpu.register[0xF] = 1
			}
			// fmt.Printf("Added V%d and V%d to %d", x, y, cpu.register[x])
			cpu.register[x] += cpu.register[y]
			cpu.pc += 2
			break
		case 0x5: // SUB Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4
			cpu.register[0xF] = 0
			if cpu.register[x] > cpu.register[y] {
				cpu.register[0xF] = 1
			}
			cpu.register[x] -= cpu.register[y]
			// fmt.Printf("Substracted V%d from V%d to %d", y, x, cpu.register[x])
			cpu.pc += 2
			break
		case 0x6: // SHR Vx {, Vy}
			x := uint16(cpu.opcode&0x0F00) >> 8
			cpu.register[0xF] = 0
			if cpu.register[x]&0x1 == 1 {
				cpu.register[0xF] = 1
			}
			cpu.register[x] >>= 1
			// fmt.Printf("Right Shift V%d to %d", x, cpu.register[x])
			cpu.pc += 2
			break
		case 0x7: // SUBN Vx, Vy
			x := uint16(cpu.opcode&0x0F00) >> 8
			y := uint16(cpu.opcode&0x00F0) >> 4
			cpu.register[0xF] = 0
			if cpu.register[y] > cpu.register[x] {
				cpu.register[0xF] = 1
			}
			cpu.register[x] = cpu.register[y] - cpu.register[x]
			// fmt.Printf("Substracted V%d from V%d to %d", x, y, cpu.register[x])
			cpu.pc += 2
			break
		case 0xE: // SHL Vx {, Vy}
			x := uint16(cpu.opcode&0x0F00) >> 8
			cpu.register[0xF] = 0
			if (cpu.register[x]&0x80)>>7 == 1 {
				cpu.register[0xF] = 1
			}
			cpu.register[x] <<= 1
			// fmt.Printf("Left Shift V%d to %d", x, cpu.register[x])
			cpu.pc += 2
			break
		}
	case 0x9000: // SNE Vx, Vy (Skip Not Equal)
		x := uint16(cpu.opcode&0x0F00) >> 8
		y := uint16(cpu.opcode&0x00F0) >> 4
		if cpu.register[x] != cpu.register[y] {
			cpu.pc += 4
		} else {
			cpu.pc += 2
		}
		break
	case 0xA000:
		cpu.I = cpu.opcode & 0x0FFF
		// fmt.Printf("Loaded 0x%x into I\n", cpu.I)
		cpu.pc += 2
		break

	case 0xC000:
		x := uint8(cpu.opcode & 0x0F00 >> 8)
		kk := uint8(cpu.opcode & 0x00FF)
		// TODO better random
		rNum := uint8(rand.Intn(255))
		cpu.register[x] = rNum & kk
		// fmt.Printf("Stored %d into V%X\n", cpu.register[x], x)
		cpu.pc += 2
		break
	case 0xD000:
		x := cpu.register[uint16(cpu.opcode&0x0F00)>>8]
		y := cpu.register[uint16(cpu.opcode&0x00F0)>>4]
		n := cpu.opcode & 0x000F

		cpu.register[0xF] = 0
		for i := uint16(0); i < n; i++ {
			pixel := cpu.memory[cpu.I+i]
			for xLine := uint16(0); xLine < 8; xLine++ {
				if (pixel & (0x80 >> xLine)) != 0 {
					if cpu.Vram[(uint16(x)+xLine+(uint16(y)+i)*64%2048)] == 1 {
						cpu.register[0xF] = 1
					}
					cpu.Vram[uint16(x)+xLine+((uint16(y)+i)*64)%2048] ^= 1
				}
			}
		}
		cpu.DrawFlag = true
		// fmt.Printf("Updated Vram\n")
		cpu.pc += 2
		break
	case 0xE000:
		switch cpu.opcode & 0x00FF {
		case 0x9E: // SKP Vx
			x := (cpu.opcode & 0x0F00) >> 8
			if cpu.Keys[cpu.register[x]] != 0 {
				cpu.pc += 2
			}
			cpu.pc += 2
			// fmt.Printf("\n")
			break
		case 0xA1: // SKNP Vx
			x := (cpu.opcode & 0x0F00) >> 8
			if cpu.Keys[cpu.register[x]] == 0 {
				cpu.pc += 2
			}
			cpu.pc += 2
			// fmt.Printf("\n")
			break
		default:
			// fmt.Printf("Unkown opcode: 0x%x\n", cpu.opcode)
		}
		break
	case 0xF000:
		switch cpu.opcode & 0x00FF {
		case 0x07:
			x := (cpu.opcode & 0x0F00) >> 8
			cpu.register[x] = cpu.delayTimer
			// fmt.Printf("Stored delayTimer %d in V%X\n", cpu.delayTimer, x)
			cpu.pc += 2
			break
		case 0x15:
			x := (cpu.opcode & 0x0F00) >> 8
			cpu.delayTimer = cpu.register[x]
			// fmt.Printf("Set delayTimer to %d\n", cpu.register[x])
			cpu.pc += 2
			break
		case 0x18:
			x := (cpu.opcode & 0x0F00) >> 8
			cpu.soundTimer = cpu.register[x]
			// fmt.Printf("Set soundTimer to %d\n", cpu.register[x])
			cpu.pc += 2
			break
		case 0x1E:
			x := (cpu.opcode & 0x0F00) >> 8
			cpu.I = cpu.I + uint16(cpu.register[x])
			cpu.pc += 2
			fmt.Printf("Hello")
			break
		case 0x29:
			var x = uint16(cpu.opcode&0x0F00) >> 8
			cpu.I = uint16(cpu.register[x] * 5)
			// fmt.Printf("Set I to 0x%x\n", cpu.I)
			cpu.pc += 2
			break
		case 0x33:
			var x = uint16(cpu.opcode&0x0F00) >> 8
			cpu.memory[cpu.I] = cpu.register[x] / 100
			cpu.memory[cpu.I+1] = (cpu.register[x] / 10) % 10
			cpu.memory[cpu.I+2] = (cpu.register[x] % 100) % 10
			cpu.pc += 2
			fmt.Printf("%d %d %d\n", cpu.memory[cpu.I], cpu.memory[cpu.I+1], cpu.memory[cpu.I+2])
			fmt.Printf("Loaded %d into memory at position 0x%x\n", cpu.register[x], cpu.I)
			break
		case 0x55: // LD [I], Vx
			x := (cpu.opcode & 0x0F00) >> 8
			for i := uint16(0); i <= x; i++ {
				cpu.memory[cpu.I+i] = cpu.register[i]
			}
			cpu.pc += 2
			break
		case 0x65:
			x := (cpu.opcode & 0x0F00) >> 8
			for i := uint16(0); i <= x; i++ {
				cpu.register[i] = cpu.memory[cpu.I+i]
				fmt.Printf("(0x%x) %d", cpu.opcode, cpu.register[i])
			}
			cpu.pc += 2
			// fmt.Printf("Updated %d registers from memory\n", vx)
			break
		default:
			fmt.Printf("Unkown opcode: 0x%x\n", cpu.opcode)
		}
		break
	default:
		fmt.Printf("Unkown opcode: 0x%x\n", cpu.opcode)
	}

	if cpu.delayTimer > 0 {
		cpu.delayTimer--
	}

	if cpu.soundTimer > 0 {
		cpu.soundTimer--
	}
}
