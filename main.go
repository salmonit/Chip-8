package main

import (
	"fmt"
	"os"

	"gitlab.com/salmonit/Chip-8/pkg/chip8"
	"korok.io/korok/gfx/dbg"
	"korok.io/korok/hid/input"

	"korok.io/korok"
	"korok.io/korok/game"
)

type MainScene struct {
	cpu *chip8.Chip8
	gfx [64 * 32]uint8
}

// Load
func (m *MainScene) Load() {
	input.RegisterButton("0", input.X)
	input.RegisterButton("1", input.One)
	input.RegisterButton("2", input.Two)
	input.RegisterButton("3", input.Three)
	input.RegisterButton("4", input.Q)
	input.RegisterButton("5", input.W)
	input.RegisterButton("6", input.E)
	input.RegisterButton("7", input.A)
	input.RegisterButton("8", input.S)
	input.RegisterButton("9", input.D)
	input.RegisterButton("A", input.Y)
	input.RegisterButton("B", input.C)
	input.RegisterButton("C", input.Four)
	input.RegisterButton("D", input.R)
	input.RegisterButton("E", input.F)
	input.RegisterButton("F", input.V)
}

// OnEnter ...
func (m *MainScene) OnEnter(g *game.Game) {
	m.cpu = m.cpu.Initialize()
	loaded := m.cpu.LoadRom("res/pong.rom")
	if !loaded {
		fmt.Println("Should exit")
		os.Exit(1)
	}
}

// Update ...
func (m *MainScene) Update(dt float32) {

	handleInput(m.cpu)

	m.cpu.EmulateCycle()

	for y := 0; y < 32; y++ {
		for x := 0; x < 64; x++ {
			if m.gfx[y*64+x] == 1 {
				dbg.DrawRect(float32(x*10), float32((31-y)*10), float32(10), float32(10))
			}
		}
	}

	if m.cpu.DrawFlag {
		m.gfx = m.cpu.Vram
		m.cpu.DrawFlag = false
	}
}

// OnExit ...
func (*MainScene) OnExit() {
	return
}

func main() {
	options := &korok.Options{
		Title:   "Chip8 Emulator",
		Width:   640,
		Height:  320,
		NoVsync: false,
	}
	korok.Run(options, &MainScene{})
}

func handleInput(cpu *chip8.Chip8) {
	if input.Button("0").Down() {
		cpu.Keys[0] = 1
	} else {
		cpu.Keys[0] = 0
	}
	if input.Button("1").Down() {
		cpu.Keys[1] = 1
	} else {
		cpu.Keys[1] = 0
	}
	if input.Button("2").Down() {
		cpu.Keys[2] = 1
	} else {
		cpu.Keys[2] = 0
	}
	if input.Button("3").Down() {
		cpu.Keys[3] = 1
	} else {
		cpu.Keys[3] = 0
	}
	if input.Button("4").Down() {
		cpu.Keys[4] = 1
	} else {
		cpu.Keys[4] = 0
	}
	if input.Button("5").Down() {
		cpu.Keys[5] = 1
	} else {
		cpu.Keys[5] = 0
	}
	if input.Button("6").Down() {
		cpu.Keys[6] = 1
	} else {
		cpu.Keys[6] = 0
	}
	if input.Button("7").Down() {
		cpu.Keys[7] = 1
	} else {
		cpu.Keys[7] = 0
	}
	if input.Button("8").Down() {
		cpu.Keys[8] = 1
	} else {
		cpu.Keys[8] = 0
	}
	if input.Button("9").Down() {
		cpu.Keys[9] = 1
	} else {
		cpu.Keys[9] = 0
	}
	if input.Button("A").Down() {
		cpu.Keys[0xA] = 1
	} else {
		cpu.Keys[0xA] = 0
	}
	if input.Button("B").Down() {
		cpu.Keys[0xB] = 1
	} else {
		cpu.Keys[0xB] = 0
	}
	if input.Button("C").Down() {
		cpu.Keys[0xC] = 1
	} else {
		cpu.Keys[0xC] = 0
	}
	if input.Button("D").Down() {
		cpu.Keys[0xD] = 1
	} else {
		cpu.Keys[0xD] = 0
	}
	if input.Button("E").Down() {
		cpu.Keys[0xE] = 1
	} else {
		cpu.Keys[0xE] = 0
	}
	if input.Button("F").Down() {
		cpu.Keys[0xF] = 1
	} else {
		cpu.Keys[0xF] = 0
	}
}
